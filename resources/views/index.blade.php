<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Quotes.com</title>
        <link rel="stylesheet" href="{{ secure_asset('css/app.css') }}">
    </head>
    <body>
        <div id="app">
            <quotes></quotes>
        </div>
        <script src="{{ secure_asset('js/app.js') }}"></script>
    </body>
</html>
