<?php

namespace App\Http\Controllers;

use App\Models\Quote;
use App\Http\Resources\Quote  as QuoteResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Get articles
        $quotes = Quote::with('tags')->orderBy('created_at', 'desc')->paginate(10);

        // Return collection of articles as a resource
        return QuoteResource::collection($quotes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return QuoteResource
     */
    public function store(Request $request)
    {
        $quote = new Quote();
        $quote->author = $request->json('author');
        $quote->text = $request->json('text');
        if($quote->save()) {
            $tags = $request->json('tags');
            foreach ($tags as $tag) {
                DB::table('quote_tags')->insert([
                    'quote_id' => $quote->id,
                    'tag_id' => $tag['id']
                ]);
            }
            return new QuoteResource($quote);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
